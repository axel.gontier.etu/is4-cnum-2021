# Illustration naïve de l'interpolation de Hermite.
# On se donne n+1=3 points (x_i,y_i) et trois pentes p_i (i = 0,1,2)
# On montre qu'il est possible de construire n=2 cubiques s_0 et s_1 dont
#     les graphes passent par les trois points en respectant les pentes p_i
# La méthode consiste à résoudre un système de 4*n équations linéaires à 4*n inconnues

import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

Tx = np.array ([1,3,4], dtype=np.float64)
Ty = np.array ([3,2,5], dtype=np.float64)
Tp = np.array ([1,-1,1], dtype=np.float64)

n = 2
A = np.zeros ([4*n,4*n], dtype=np.float64)
b = np.zeros (4*n, dtype=np.float64)

# le graphe de s_0 passe par (x_i,y_i) pour i = 0,1
A[0:2,0:4] = np.array ([[x**3, x**2, x, 1] for x in Tx[0:2]])
b[0:2] = Ty[0:2]

# le graphe de s_1 passe par (x_i,y_i) pour i = 1,2
A[2:4,4:8] = np.array ([[x**3, x**2, x, 1] for x in Tx[1:3]])
b[2:4] = Ty[1:3]

# la dérivée première de s_0 est égale à p_i en x_i pour i = 0,1
A[4:6,0:4] = np.array ([[3*x**2, 2*x, 1, 0] for x in Tx[0:2]])
b[4:6] = Tp[0:2]

# la dérivée première de s_1 est égale à p_i en x_i pour i = 1,2
A[6:8,4:8] = np.array ([[3*x**2, 2*x, 1, 0] for x in Tx[1:3]])
b[6:8] = Tp[1:3]

a = nla.solve (A, b)

# La fonction s(x) est définie par morceaux. 
# Son graphe est globalement C1

def s (x) :
    if x < Tx[1] :
        return ((a[0]*x + a[1])*x + a[2])*x + a[3]
    else :
        return ((a[4]*x + a[5])*x + a[6])*x + a[7]

plt.scatter (Tx, Ty)
h = .5
for i in range (n+1) :
    plt.plot ([Tx[i]-h,Tx[i]+h], [Ty[i]-h*Tp[i],Ty[i]+h*Tp[i]])

xplot = np.linspace (Tx[0]-.1*h, Tx[n]+.1*h, 100)
yplot = [s(x) for x in xplot]
plt.plot (xplot,yplot)
plt.show ()



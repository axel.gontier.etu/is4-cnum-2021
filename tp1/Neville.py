# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np

def Neville(tx,ty,x):

    n=np.size(tx)
    m=np.empty([n,n])
    for i in range(n):# on initialise la premiere colonne avec les yi
        m[i,0]=ty[i]
    for j in range(1,n):
        for i in range(n-j):
            
            m[i,j]=(   (tx[i]-x)*m[i+1,j-1]+    (x-tx[i+j])*m[i,j-1]     ) /(tx[i]-tx[i+j])
            
    return m[0,n-1]

tx=np.array([1,2,3,5])
ty=np.array([1,4,2,5])        
  
print(Neville(tx,ty,4))      

#question 2

import matplotlib.pyplot as plt
from scipy.interpolate import BarycentricInterpolator
f=BarycentricInterpolator(tx,ty)


plt.scatter(tx,ty)


 
# résultats de la fonction interpolation f sur de nouvelles données
new_x = np.linspace(0, 6, 50)
result = f(new_x)
 

plt.plot(new_x, result,c='r')
plt.plot(4,Neville(tx,ty,4))
plt.ylim(-10,20)
plt.show()
        
    

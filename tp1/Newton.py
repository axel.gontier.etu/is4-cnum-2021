import numpy as np
def Newton(Tx,Ty,x):
    n=np.size(Tx)
    
    C=np.empty([n,n])
    
    for i in range(n):# on initialise la premiere colonne avec les yi
        C[i,0]=Ty[i]
    for j in range(1,n):
        for i in range(n-j):
            C[i,j]=(C[i+1,j-1]-C[i,j-1])/(Tx[i+j]-Tx[i])
            
    
    n-=1
    res=C[0,n]

    for i in range(n-1,-1,-1):
        res*=(x-Tx[n-i])
        res+=C[n-i,i]
        
    return res
tx=np.array([1,2,3,5])
ty=np.array([1,4,2,5])        
  
print(Newton(tx,ty,4))   

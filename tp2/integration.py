#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  3 13:54:49 2021

@author: agontie1
"""


import math
#question 1
def relerr(I,Ip):
    try:
        return abs(I-Ip)/abs(I)
    except ZeroDivisionError :
        print("Erreur division par zéro")
#question 2       
def nbbits(I,Ip):
    return - math.log2(relerr(I,Ip))
#question 3
#il doit y avoir 10 bits
#print(nbbits(1024,1023))
#print(relerr(1024,1023))


import numpy as np
import math
def p1(x):
    return math.exp(1/math.log(2+x)+math.sin(math.sqrt(x))+1)

def f1(x):
    t1 = 2 + x
    t2 = math.log(t1)
    t3 = t2 ** 2
    t7 = math.sqrt(x)
    t9 = math.cos(t7)
    t14 = math.sin(t7)
    t16 = math.exp(0.1e1 / t2 + t14 + 1)
    t17 = (-0.1e1 / t3 / t1 + 0.1e1 / t7 * t9 / 2) * t16
    return t17

def f2(x):
    return 2/(2*x**2-2*x+1)

def p2(x):
    return 2*math.atan(2*x-1)



#question 6 


def trapezes(f,a,b,N):
    h=(b-a)/N
    liste_abs=[i for i in np.arange(a,b,h)]
    liste_ord=[f(i)for i in liste_abs]
  # print(liste_abs)
    #print(liste_ord)
    #calcul de l'approximation
    I=h*(sum(liste_ord[1:-1])+0.5*(liste_ord[0]+liste_ord[-1]))
    return I


#question 7

def simpson(f,a,b,N):
    h=(b-a)/N
    liste_abs=[i for i in np.arange(a,b,h)]
    liste_ord=[f(i)for i in liste_abs]
    #creons la liste des coeff de la forme 1,4,2,4,4,2,1
    i=0;
    l=[]
    while(i<N/2):
        
        if(i==0):
            l.append(1)
        elif(i%2!=0):
            l.append(4)
        else:
            l.append(2)
        i+=1
    while(i<N):
        if(i==N-1):
            l.append(1)
        elif(i%2!=0):
            l.append(2)
        else:
            l.append(4)
        i+=1
    I=0
    for i in range(len(l)):
        I+= (h/3)*liste_ord[i]*l[i]
    return I
    
        
        
print(trapezes(f1,1,10,10000))
print(simpson(f1,1,10,10000))
print(p1(10)-p1(1))

#question 8
import matplotlib.pyplot as plt

for k in range(2,17):
        plt.scatter(k,nbbits(p1(10)-p1(1),trapezes(f1,1,10,2**k)),c='r')
        plt.scatter(k,nbbits(p1(10)-p1(1),simpson(f1,1,10,2**k)),c='pink')
        plt.scatter(k,nbbits(p2(10)-p2(1),trapezes(f2,1,10,2**k)),c='b')
        plt.scatter(k,nbbits(p2(10)-p2(1),simpson(f2,1,10,2**k)),c='g')
plt.show()
        
        
    
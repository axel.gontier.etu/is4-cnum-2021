#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 13:51:18 2021

@author: agontie1
"""



import numpy as np
import csv
import scipy.linalg as nla

# Les données sont extraites de 'spnbmd.csv'
# 1. Construction de result = un dictionnaire contenant des valeurs
#       a: [d1, d2, ..., dn] (liste non vide)
#    où la clé a est un âge (flottant) et d1, d2, ..., dn sont les densités
#    des patients d'âge a. 

result = { }
with open ('spnbmd.csv') as f:
    reader = csv.DictReader(f, delimiter=',')
    for row in reader:
        if (row['ethnic'] in ['Hispanic'] ) and (row['sex']=="mal"):
            age = np.float64(row['age'])
            densite = np.float64(row['spnbmd'])
            if age in result :
                result[age].append (densite)
            else :
                result[age] = [ densite ]

# 2. Construction des tableaux x (trié par ordre croissant) et y
#   x[i] = un âge
#   y[i] = la moyenne des densités pour l'âge x[i]

x = np.array(sorted(result))
y = np.array([np.average(result[age]) for age in x], dtype=np.float64)

# les points sont indicés de 0 à n (inclus) comme dans le support de cours
n = x.shape[0] - 1


test={key: value for key, value in result.items() if key<10.5}


#1.2 calcul spline lissante

x=np.array(sorted([1,7,3,2,4,6]))
y=np.array([2,9,6,4,8,3])
p=1
def h(i):
    return x[i+1]-x[i]

#construction matrice T
n=5
T=np.zeros((n-1,n-1))
# on remplie les deux diagonales "presque centrale"


for i in range(1,n-1,1):
    T[i,i-1]=h(i)
    T[i-1,i]=h(i)
    
#remplissage de la diagonale
for i in range(n-1):
    T[i,i]=2*(h(i)+h(i+1))
T=(1/3)*T

#construction matrice  Q


Q=np.zeros((n+1,n-1))

# on remplie les deux diagonales "presque centrale"

for i in range(n-1):#diago la plus basse par g1
    Q[i+2,i]=1/h(i+1)

for i in range(n-1):#diago au millieu commencant par -go-g1
    Q[i+1,i]=-(1/h(i))-(1/h(i+1))

for i in range(n-1): #diago commencant par go 
    Q[i,i]=1/h(i)

np.transpose(Q).dot(np.eye(n+1))
print(T)
print("\n")
print(Q)
#5
c=nla.solve(np.transpose(Q).dot(np.eye(n+1)**2).dot(Q)+ p * T,p*np.transpose(Q).dot(y))
#ajout des conditions c0 et cn
c=np.concatenate((np.array([0]),c,np.array([0])))
print(c)
print(c[1:n])

#6
print(f"c:{c}")
a=y-(1/p)*(np.eye(n+1)**2).dot(Q).dot(c[1:n])

print(f"a:{a}")
#calcul de d
d = np.zeros(n-1)
for i in range(n-1):
    d[i]=(c[i+1]-c[i])/(3*h(i))
d=np.concatenate((np.array([0]),d,np.array([0])))
print(f"d:{d}")
#calcul de b 
b = np.zeros(n-1)
for  i in range(n-1):
    b[i]=((a[i+1]-a[i])/h(i))-c[i]*h(i)-d[i]*h(i)**2
b=np.concatenate((np.array([0]),b,np.array([0])))
print(f"b:{b}")


#7
x=np.array(sorted([1,7,3,2,4,6]))
def eval_spline(z,x):
    
    i=np.searchsorted(x,z,side='right')
    
    if(i<len(x) and i!=0):
        return ((d[i]*(z-x[i])+c[i])*(z-x[i])+b[i])*(z-x[i])+a[i]
    
eval_spline(2.001,x)


#8
import matplotlib.pyplot as plt
plt.scatter(x,y)
new_x = np.linspace(1, 7, 500)
new_y=np.zeros(500)
j=0
for i in  np.linspace(1, 7, 500):
    new_y[j]=eval_spline(i,x)
    j+=1
#print(new_y)
plt.plot(new_x, new_y,c='r')
plt.show()







   




